FROM python:3.11.6-slim

RUN apt update && apt install -y \
    make \
    gcc \
    python3-dev \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*


## Install poetry
RUN pip install -U pip setuptools
RUN pip install poetry

ENV PYTHONUNBUFFERED=1
ENV POETRY_VIRTUALENVS_IN_PROJECT=1

WORKDIR /workspace
COPY poetry.lock pyproject.toml /workspace/
RUN poetry install --no-interaction --ansi

COPY . /workspace

# Creates a non-root user with an explicit UID and adds permission to access the /workspace folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /workspace
USER appuser

EXPOSE 8000

CMD ["poetry", "run", "uvicorn", "app.main:app", "--host=0.0.0.0", "--port=8000"]
