
install:
	poetry install

dev:
	poetry run uvicorn app.main:app --reload --port=8000

# Usage make createmigration name="migration_name"
createmigration:
	poetry run alembic revision -m "$(name)"

migrate:
	poetry run alembic upgrade head

web-dev:
	cd frontend && npm run dev

web-build:
	cd frontend && npm run build
