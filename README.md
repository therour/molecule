# Molecule Information

## Get Started

### Run with docker-compose

1. You can run the services by running:
```
docker-compose up -d
```
2. Run migration
```
docker-compose run --rm backend poetry run alembic upgrade head
```
3. Open the app in browser. default one will be on http://localhost:3000, and backend will be published to http://localhost:8000


### Manual Installation

#### Requirements

1. Python >= 3.11
2. Poetry package manager
3. Postgresql
4. Node >= 18

#### Backend
1. Install dependency
```
poetry install
```

2. Setup env,
```
cp .env.example .env
```

3. Run migration
```
poetry run alembic upgrade head
```

4. Run server

```
poetry run uvicorn app.main:app --reload --port=8000
```

#### Frontend

1. get into `frontend` directory

```
cd frontend
```

2. Install dependency

```
npm install
```

3. Setup env, you only need to set `VITE_API_URL`
```
cp .env.example .env
```

3. Run development

```
npm run dev
```
