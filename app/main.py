from typing import Annotated

from fastapi import APIRouter, Depends, FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.exception_handlers import request_validation_exception_handler
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse

from app.settings import Settings, get_settings
from app.utils.exception import NotFoundException, SimpleValidationError

from .molecules.router import router as molecule_router

settings = get_settings()
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[settings.server.cors_origin],
    allow_methods=["*"],
    allow_headers=["*"],
)


## ROUTER REGISTRATION


api_router = APIRouter(prefix="/api")
api_router.include_router(molecule_router)
app.include_router(api_router)


@app.get("/")
async def root(settings: Annotated[Settings, Depends(get_settings)]):
    return {"message": "Hello World", "app_name": settings.app_name}


## EXCEPTION HANDLERS


@app.exception_handler(NotFoundException)
async def not_found_exception_handler(request, exc: NotFoundException):
    return JSONResponse(
        status_code=404,
        content={"message": exc.message},
    )


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc: RequestValidationError):
    formatted_errors = exc.errors()
    if not isinstance(exc, SimpleValidationError):
        formatted_errors = {}
        for error in exc.errors():
            error_dict = formatted_errors
            for path in error["loc"][1:]:
                error_dict = error_dict.setdefault(path, {})
            error_dict[error["loc"][-1]] = error["msg"]
            formatted_errors = {**formatted_errors, **error_dict}

    return JSONResponse(
        status_code=422,
        content={
            "errors": formatted_errors,
            "message": "Validation error",
        },
    )
