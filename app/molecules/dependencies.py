from fastapi import Depends
from sqlalchemy.orm import Session


from app.dependencies import get_db
from .service import MoleculeService


def get_molecule_service(db: Session = Depends(get_db)):
    return MoleculeService(db)
