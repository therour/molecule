class InvalidMoleculeSmiles(Exception):
    def __init__(self, smiles):
        self.smiles = smiles
        super().__init__(f"Invalid SMILES: {smiles}")
