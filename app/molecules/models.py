from sqlalchemy import Column, Float, Integer, String, Text, text

from app.database import Base


class Molecule(Base):
    __tablename__ = "molecules"

    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    smiles = Column(String, unique=True, nullable=False)
    name = Column(
        String(255),
        nullable=False,
        server_default=text("'molecule_' || nextval('molecule_name_seq')::text"),
    )
    description = Column(Text, nullable=True)
    log_p = Column(Float, nullable=False)
    number_of_atoms = Column(Integer, nullable=False)
    molecular_weight = Column(Float, nullable=False)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
