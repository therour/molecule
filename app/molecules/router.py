import io
import re
from typing import Annotated, Dict

from fastapi import APIRouter, Depends, Query, Request, Response, UploadFile
from fastapi.responses import JSONResponse

from app.utils.exception import SimpleValidationError
from app.utils.schema import ModelPagination

from .dependencies import get_molecule_service
from .schema import (
    BulkDeletePayload,
    Molecule,
    MoleculeCreate,
    MoleculeFilterQuery,
    MoleculeUpdate,
)
from .service import MoleculeService

router = APIRouter(prefix="/molecules")


@router.get("/", tags=["molecules"], response_model=ModelPagination[Molecule])
async def get_molecules(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    request: Request,
    page: int = 1,
    size: Annotated[int, Query(le=100)] = 10,
    sort: str = None,
    search: Annotated[str, Query(max_length=100)] = "",
):
    q_filter = MoleculeFilterQuery(**request.query_params._dict)
    return service.get_paginated(
        page=page, size=size, filter=q_filter, sort=sort, search=search
    )


@router.get("/{id}", tags=["molecules"])
async def get_molecule(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    id: int,
):
    molecule = service.get(id)
    return {"data": molecule}


@router.post("/", tags=["molecules"])
async def create_molecule(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    item: MoleculeCreate,
):
    molecule = service.create(item)
    return JSONResponse(status_code=201, content={"data": molecule.as_dict()})


@router.put("/{id}", tags=["molecules"])
async def update_molecule(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    id: int,
    item: MoleculeUpdate,
):
    molecule = service.update(id, item)
    return {"data": molecule}


@router.delete("/{id}", tags=["molecules"])
async def delete_molecule(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    id: int,
):
    service.delete(id)
    return Response(status_code=204)


@router.post("/bulk-delete", tags=["molecules"])
async def bulk_delete_molecules(
    service: Annotated[MoleculeService, Depends(get_molecule_service)],
    payload: BulkDeletePayload,
):
    service.bulk_delete(payload.ids)
    return Response(status_code=204)


@router.post("/import", tags=["molecules"])
async def import_molecule(
    service: Annotated[MoleculeService, Depends(get_molecule_service)], file: UploadFile
):
    with file.file as f:
        try:
            results = service.bulk_create_from_file(f, batch_size=50)
        except UnicodeDecodeError:
            raise SimpleValidationError({"file": "Invalid file format"})

    return {"data": results}
