from typing import Annotated, Any, Optional

from pydantic import BaseModel, ConfigDict, Field

from app.utils.filter_query import BaseFilterModel, FilterQuery


class MoleculeFilterQuery(BaseFilterModel):
    name: FilterQuery[str] | None = None
    smiles: FilterQuery[str] | None = None
    log_p: FilterQuery[float] | None = None
    number_of_atoms: FilterQuery[int] | None = None
    molecular_weight: FilterQuery[float] | None = None


class BaseMolecule(BaseModel):
    smiles: str
    description: Optional[str] = None


class Molecule(BaseMolecule):
    id: int
    name: str
    log_p: float
    number_of_atoms: int
    molecular_weight: float

    model_config = ConfigDict(
        from_attributes=True,
    )


class MoleculeCreate(BaseModel):
    smiles: Annotated[
        str,
        Field(pattern=r"^([^J][A-Za-z0-9@+\-\[\]\.\(\)\\\/%=#$]+)$"),
    ]
    name: Optional[str] = None
    description: Optional[str] = None


class MoleculeUpdate(BaseModel):
    name: str
    description: Optional[str] = None


class BulkDeletePayload(BaseModel):
    ids: list[int]
