import io
from typing import BinaryIO, TypedDict

import psycopg
import rdkit.Chem as Chem
from rdkit.Chem import Descriptors
from sqlalchemy import exc as sqlexc
from sqlalchemy import insert
from sqlalchemy.orm import Session

from app.utils.exception import NotFoundException, SimpleValidationError
from app.utils.filter_query import apply_filter_query
from app.utils.schema import ModelPagination

from . import schema
from .models import Molecule


class BulkCreateResult(TypedDict):
    already_exists: list[str]
    success_count: list[str]
    invalid: list[str]


class SmileProperties(TypedDict):
    number_of_atoms: int
    molecular_weight: float
    log_p: float


def get_smile_properties(smiles: str) -> SmileProperties | None:
    if (lig := Chem.MolFromSmiles(smiles)) is None:
        return None

    properties = {
        "number_of_atoms": lig.GetNumAtoms(),
        "molecular_weight": Descriptors.ExactMolWt(lig),
        "log_p": Descriptors.MolLogP(lig),
    }
    return properties


class MoleculeService:
    def __init__(self, db: Session):
        self.db = db

    def create(self, molecule: schema.MoleculeCreate):
        if self.db.query(Molecule).filter_by(smiles=molecule.smiles).first():
            raise SimpleValidationError(
                {"smiles": "Molecule with this formula is already exists"}
            )

        properties = get_smile_properties(molecule.smiles)
        if properties is None:
            raise SimpleValidationError({"smiles": "Invalid SMILES formula"})

        db_molecule = Molecule(
            name=molecule.name,
            smiles=molecule.smiles,
            description=molecule.description,
            log_p=properties.get("log_p"),
            number_of_atoms=properties.get("number_of_atoms"),
            molecular_weight=properties.get("molecular_weight"),
        )
        self.db.add(db_molecule)
        self.db.commit()
        self.db.refresh(db_molecule)
        return db_molecule

    def get(self, id: int):
        if data := self.db.query(Molecule).filter_by(id=id).first():
            return data
        raise NotFoundException("Molecule not found")

    def get_paginated(
        self,
        filter: schema.MoleculeFilterQuery = None,
        search=None,
        sort: str | None = None,
        page=1,
        size=10,
    ):
        query = self.db.query(Molecule)
        if search:
            query = query.filter(Molecule.name.ilike(f"%{search}%"))

        if sort:
            if sort.startswith("-"):
                query = query.order_by(getattr(Molecule, sort[1:]).desc())
            else:
                query = query.order_by(getattr(Molecule, sort).asc())
        else:
            query = query.order_by(Molecule.id.asc())

        if filter:
            query = apply_filter_query(Molecule, query, filter)

        total = query.count()

        query = query.limit(size).offset((page - 1) * size)
        data = query.all()

        return ModelPagination(data=data, total=total)

    def update(self, id: int, payload: schema.MoleculeUpdate):
        if data := self.db.query(Molecule).filter_by(id=id).first():
            data.name = payload.name
            data.description = payload.description
            self.db.commit()
            self.db.refresh(data)
            return data
        raise NotFoundException("Molecule not found")

    def delete(self, id: int) -> None:
        if data := self.db.query(Molecule).filter_by(id=id).first():
            self.db.delete(data)
            self.db.commit()
            return
        raise NotFoundException("Molecule not found")

    def bulk_delete(self, ids: list[int]) -> None:
        self.db.query(Molecule).filter(Molecule.id.in_(ids)).delete(
            synchronize_session=False
        )
        self.db.commit()

    def bulk_create_from_file(
        self, file: BinaryIO, batch_size: int = 50
    ) -> BulkCreateResult:
        def _process_batch(data: list[schema.MoleculeCreate]) -> BulkCreateResult:
            existing_smiles = [
                row[0]
                for row in self.db.query(Molecule.smiles)
                .filter(Molecule.smiles.in_(m.smiles for m in data))
                .all()
            ]
            to_create_molecules = [m for m in data if m.smiles not in existing_smiles]
            invalid_smiles = []

            insert_values = []
            insert_values_without_name = []
            for item in to_create_molecules:
                if (properties := get_smile_properties(item.smiles)) is None:
                    invalid_smiles.append(item.smiles)
                    continue

                value = {
                    "name": item.name,
                    "smiles": item.smiles,
                    "log_p": properties.get("log_p"),
                    "number_of_atoms": properties.get("number_of_atoms"),
                    "molecular_weight": properties.get("molecular_weight"),
                }
                if value.get("name") is None:
                    del value["name"]
                    insert_values_without_name.append(value)
                else:
                    insert_values.append(value)

            if len(insert_values) > 0:
                self.db.execute(insert(Molecule).values(insert_values))
            if len(insert_values_without_name) > 0:
                self.db.execute(insert(Molecule).values(insert_values_without_name))

            return {
                "already_exists": existing_smiles,
                "success_count": len(insert_values) + len(insert_values_without_name),
                "invalid": invalid_smiles,
            }

        unique_smiles = set()
        batch = []
        total_results: BulkCreateResult = {
            "already_exists": [],
            "invalid": [],
            "success_count": 0,
        }

        for line in io.TextIOWrapper(file, encoding="utf-8"):
            name = smiles = None
            name_and_smiles = str(line).rsplit(maxsplit=1)
            if len(name_and_smiles) > 1:
                name, smiles = name_and_smiles
            else:
                smiles = name_and_smiles[0]

            if smiles in unique_smiles:
                continue

            batch.append(schema.MoleculeCreate(name=name, smiles=smiles))
            unique_smiles.add(smiles)

            if len(batch) >= batch_size:
                res = _process_batch(batch)
                total_results.get("already_exists").extend(res.get("already_exists"))
                total_results.get("invalid").extend(res.get("invalid"))
                total_results["success_count"] += res.get("success_count")
                batch.clear()

        if len(batch) > 0:
            res = _process_batch(batch)
            total_results.get("already_exists").extend(res.get("already_exists"))
            total_results.get("invalid").extend(res.get("invalid"))
            total_results["success_count"] += res.get("success_count")
            batch.clear()

        self.db.commit()

        return total_results
