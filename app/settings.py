from functools import lru_cache

from pydantic_settings import BaseSettings, SettingsConfigDict


class ServerSettings(BaseSettings):
    cors_origin: str


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file=".env", env_nested_delimiter="__")

    app_name: str = "Awesome API"

    server: ServerSettings

    db_url: str


@lru_cache
def get_settings():
    return Settings()
