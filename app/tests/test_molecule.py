from pathlib import Path

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

from app.database import Base
from app.dependencies import get_db
from app.main import app
from app.molecules.models import Molecule

SQLALCHEMY_DATABASE_URL = "postgresql+psycopg://postgres:secret@db:5432/molecules"

engine = create_engine(SQLALCHEMY_DATABASE_URL)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture
def session():
    db = TestingSessionLocal()
    Base.metadata.drop_all(bind=engine)

    db.execute(text("DROP SEQUENCE IF EXISTS molecule_name_seq;"))
    db.execute(text("CREATE SEQUENCE molecule_name_seq;"))
    db.commit()

    Base.metadata.create_all(bind=engine)

    try:
        yield db
    finally:
        db.close()


@pytest.fixture
def client(session):
    def _override_get_db():
        yield session

    app.dependency_overrides[get_db] = _override_get_db
    yield TestClient(app)


def test_get_molecules(client, session):
    session.add(
        Molecule(
            name="Molecule 1",
            smiles="CCO",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.commit()

    response = client.get("/api/molecules/")
    assert response.status_code == 200
    response_data = response.json().get("data")
    print(response_data)
    assert len(response_data) == 1
    assert response_data[0].get("name") == "Molecule 1"


def test_create_molecule(client):
    response = client.post(
        "/api/molecules/",
        json={
            "name": "Molecule 1",
            "smiles": "CCO",
        },
    )

    assert response.status_code == 201
    assert response.json().get("data").get("name") == "Molecule 1"


def test_create_molecule_without_name(client):
    response = client.post(
        "/api/molecules/",
        json={
            "smiles": "CCO",
        },
    )

    assert response.status_code == 201
    assert response.json().get("data").get("name") == "molecule_1"


def test_get_molecule(client, session):
    session.add(
        Molecule(
            name="Molecule 1",
            smiles="CCO",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.commit()

    response = client.get("/api/molecules/1")
    assert response.status_code == 200
    assert response.json().get("data").get("name") == "Molecule 1"


def test_update_molecule(client, session):
    session.add(
        Molecule(
            name="Molecule 1",
            smiles="CCO",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.commit()

    response = client.put("/api/molecules/1", json={"name": "Molecule 2"})
    assert response.status_code == 200
    assert response.json().get("data").get("name") == "Molecule 2"


def test_delete_molecule(client, session):
    session.add(
        Molecule(
            name="Molecule 1",
            smiles="CCO",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.commit()

    response = client.delete("/api/molecules/1")
    assert response.status_code == 204

    assert session.query(Molecule).count() == 0


def test_bulk_delete_molecules(client, session):
    session.add(
        Molecule(
            name="Molecule 1",
            smiles="CCO",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.add(
        Molecule(
            name="Molecule 2",
            smiles="CCC",
            log_p=1.1,
            number_of_atoms=5,
            molecular_weight=1.1,
        )
    )
    session.commit()

    response = client.post("/api/molecules/bulk-delete", json={"ids": [1, 2]})
    assert response.status_code == 204

    assert session.query(Molecule).count() == 0


def test_import_molecules(client, session):
    files = [("file", open(Path(__file__).parent / "example.smi", "rb"))]
    response = client.post(
        "/api/molecules/import",
        files=files,
    )
    assert response.status_code == 200
    assert session.query(Molecule).count() == 4
