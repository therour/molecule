from typing import Generic, List, TypeVar

T = TypeVar("T")


class PaginatedData(Generic[T]):
    data: List[T] = []
    total: int = 0

    def __init__(self, data: List[T] = [], total: int = 0) -> None:
        self.data = data
        self.total = total
