from typing import Any, Mapping

from fastapi.exceptions import RequestValidationError


class NotFoundException(Exception):
    message: str | None

    def __init__(self, message: str | None = None):
        self.message = message
        super().__init__(message)


class SimpleValidationError(RequestValidationError):
    def __init__(self, errors: Mapping[str, Any]):
        super().__init__(errors)
