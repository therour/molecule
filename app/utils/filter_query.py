from typing import TYPE_CHECKING, Generator, Generic, Optional, Tuple, TypeVar

from pydantic import BaseModel, validator
from sqlalchemy import Column, func
from sqlalchemy.orm import Query

V = TypeVar("V")


def parse_nested_query_param(input_string: str, value=None):
    nested_dict = {}
    parts = input_string.split("=")
    keys = parts[0].split("[")
    if value is None and len(parts) > 1:
        value = parts[1]

    current_dict = nested_dict

    for key in keys[:-1]:
        current_key = key.replace("]", "")
        current_dict = current_dict.setdefault(current_key, {})

    current_dict[keys[-1].replace("]", "")] = value

    return nested_dict


class FilterQuery(BaseModel, Generic[V]):
    operator: str
    value: Optional[V] = None

    @validator("value", pre=True)
    def empty_str_to_none(cls, v):
        if v == "":
            return None
        return v


class BaseFilterModel(BaseModel):
    def __init__(cls, **kwargs):
        result = {}

        def _merge(a: dict, b: dict, path=[]):
            for key in b:
                if key in a:
                    if isinstance(a[key], dict) and isinstance(b[key], dict):
                        _merge(a[key], b[key], path + [str(key)])
                    elif a[key] != b[key]:
                        raise Exception("Conflict at " + ".".join(path + [str(key)]))
                else:
                    a[key] = b[key]
            return a

        for key, value in kwargs.items():
            if key.startswith("filter"):
                parsed = parse_nested_query_param(key, value)
                _merge(result, parsed["filter"])

        return super().__init__(**result)

    if TYPE_CHECKING:

        def __iter__(self) -> Generator[Tuple[str, FilterQuery], None, None]:
            return super().__iter__()


O = TypeVar("O")


def apply_filter_query(entity: O, query: Query[O], filter: BaseFilterModel) -> Query[O]:
    for key, opt in filter:
        if opt is None or getattr(entity, key, None) is None:
            continue

        column: Column = getattr(entity, key)
        if (
            opt.operator == "contains"
            and isinstance(opt.value, str)
            and opt.value != ""
        ):
            query = query.filter(column.contains(opt.value))
        elif (
            opt.operator == "equals" and isinstance(opt.value, str) and opt.value != ""
        ):
            query = query.filter(column == opt.value)
        elif (
            opt.operator == "startsWith"
            and isinstance(opt.value, str)
            and opt.value != ""
        ):
            query = query.filter(column.startswith(opt.value))
        elif (
            opt.operator == "endsWith"
            and isinstance(opt.value, str)
            and opt.value != ""
        ):
            query = query.filter(column.endswith(opt.value))
        elif opt.operator == "isEmpty":
            query = query.filter(func.coalesce(column, "") == "")
        elif opt.operator == "isNotEmpty":
            query = query.filter(func.coalesce(column, "") != "")
        elif (
            opt.operator == "isAnyOf"
            and isinstance(opt.value, list)
            and len(opt.value) > 0
        ):
            query = query.filter(column.in_(opt.value))
        elif opt.operator == "=" and opt.value is not None:
            query = query.filter(column == opt.value)
        elif opt.operator == "!=" and opt.value is not None:
            query = query.filter(column != opt.value)
        elif opt.operator == ">" and opt.value is not None:
            query = query.filter(column > opt.value)
        elif opt.operator == ">=" and opt.value is not None:
            query = query.filter(column >= opt.value)
        elif opt.operator == "<" and opt.value is not None:
            query = query.filter(column < opt.value)
        elif opt.operator == "<=" and opt.value is not None:
            query = query.filter(column <= opt.value)

    return query
