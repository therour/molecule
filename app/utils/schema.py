from typing import Generic, TypeVar

from pydantic import BaseModel

M = TypeVar("M")


class ModelPagination(BaseModel, Generic[M]):
    total: int
    data: list[M]
