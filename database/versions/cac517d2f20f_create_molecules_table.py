"""create_molecules_table

Revision ID: cac517d2f20f
Revises:
Create Date: 2023-10-25 20:17:58.394836

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision: str = "cac517d2f20f"
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("CREATE SEQUENCE molecule_name_seq;")

    op.create_table(
        "molecules",
        sa.Column("id", sa.Integer, primary_key=True, autoincrement=True),
        sa.Column("smiles", sa.String(255), unique=True, nullable=False),
        sa.Column(
            "name",
            sa.String(255),
            nullable=False,
            server_default=sa.text("'molecule_' || nextval('molecule_name_seq')::text"),
        ),
        sa.Column("description", sa.Text, nullable=True),
        sa.Column("log_p", sa.Float, nullable=False),
        sa.Column("number_of_atoms", sa.Integer, nullable=False),
        sa.Column("molecular_weight", sa.Float, nullable=False),
    )


def downgrade() -> None:
    op.drop_table("molecules")
    op.execute("DROP SEQUENCE molecule_name_seq;")
