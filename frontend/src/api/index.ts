import Axios, { AxiosRequestConfig } from 'axios'
import { IMolecule } from '@/types'

const request = Axios.create({
  baseURL: import.meta.env.VITE_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
})

export type GetAllMoleculesParams = { page?: number; search?: string; sort?: string; filter?: unknown }
type CreateMoleculePayload = { name: string | null; description: string | null; smiles: string }
type UpdateMoleculePayload = { id: number; name: string | null; description: string | null }

const Api = {
  molecules: {
    async getAll(params?: GetAllMoleculesParams) {
      const response = await request.get<{ data: IMolecule[]; total: number }>('/molecules/', { params })
      return response.data
    },
    async find(id: number) {
      const response = await request.get<{ data: IMolecule }>(`/molecules/${id}/`)
      return response.data
    },
    async create(payload: CreateMoleculePayload) {
      const response = await request.post<{ data: IMolecule }>('/molecules/', payload)
      return response.data
    },
    async update(payload: UpdateMoleculePayload) {
      const response = await request.put<{ data: IMolecule }>(`/molecules/${payload.id}/`, payload)
      return response.data
    },
    async delete(id: number) {
      await request.delete(`/molecules/${id}/`)
    },
    async bulkDelete(ids: number[]) {
      await request.post('/molecules/bulk-delete/', { ids })
    },
    async import(file: File, opts: AxiosRequestConfig = {}) {
      const formData = new FormData()
      formData.append('file', file)
      const { headers = {} } = opts
      headers['Content-Type'] = 'multipart/form-data'

      type ImportResponse = { data: { already_exists: string[]; invalid: string[]; success_count: number } }

      const response = await request.post<ImportResponse>('/molecules/import/', formData, { headers, ...opts })
      return response.data
    },
  },
}

export default Api
