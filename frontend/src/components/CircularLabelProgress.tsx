import Box from '@mui/material/Box'
import CircularProgress, { CircularProgressProps } from '@mui/material/CircularProgress'
import Typography from '@mui/material/Typography'

type Props = CircularProgressProps & {
  value?: number
}

export default function CircularLabelProgress(props: Props) {
  if (props.variant !== 'determinate') {
    return <CircularProgress {...props} value={undefined} />
  }

  return (
    <Box sx={{ position: 'relative', display: 'inline-flex' }}>
      <CircularProgress {...props} />
      <Box
        sx={{
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: 'absolute',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Typography variant="caption" component="div" color="text.secondary">
          {props.value !== undefined ? Math.round(props.value) + '%' : ''}
        </Typography>
      </Box>
    </Box>
  )
}
