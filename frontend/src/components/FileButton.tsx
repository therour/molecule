import Button, { ButtonProps } from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import React, { Ref, useCallback } from 'react'

const VisuallyHiddenInput = styled('input')({
  clip: 'rect(0 0 0 0)',
  clipPath: 'inset(50%)',
  height: 1,
  overflow: 'hidden',
  position: 'absolute',
  bottom: 0,
  left: 0,
  whiteSpace: 'nowrap',
  width: 1,
})

type FileButtonProps = Omit<ButtonProps<'label'>, 'component' | 'onChange' | 'name'> & {
  name?: string
  onChange?: (file: File | null) => void
  inputRef?: Ref<HTMLInputElement>
}
export const FileButton = ({ children, name, onChange, inputRef, ...props }: FileButtonProps) => {
  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const file = e.currentTarget.files?.[0] ?? null
      onChange?.(file)
    },
    [onChange],
  )

  return (
    <Button {...props} component="label">
      {children}
      <VisuallyHiddenInput ref={inputRef} name={name} onChange={handleChange} type="file" />
    </Button>
  )
}

export default FileButton
