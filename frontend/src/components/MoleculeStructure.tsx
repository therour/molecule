import initRDKitModule, { type RDKitLoader } from '@rdkit/rdkit'
import clsx from 'clsx'
import { useEffect, useMemo, useState } from 'react'
import './MoleculeStructure.css'
import RDkitWasmURL from '@rdkit/rdkit/dist/RDKit_minimal.wasm?url'

const RDkitPromise = (initRDKitModule as RDKitLoader)({ locateFile: () => RDkitWasmURL }).then((instance) => {
  window.RDKit = instance
})

type Props = {
  smiles: string
  className?: string
  width: number
  height: number
}

type MoleculeSVGDetail = {
  width: number
  height: number
  extra?: Record<string, unknown>
}
const drawSVG = (smiles: string, detail: MoleculeSVGDetail) => {
  if (!window.RDKit) {
    return
  }

  const mol = window.RDKit.get_mol(smiles)
  if (!mol) {
    return 'Invalid molecule.'
  }

  const svg = mol.get_svg_with_highlights(
    JSON.stringify({
      width: detail.width,
      height: detail.height,
      bondLineWidth: 1,
      addStereoAnnotation: true,
      ...(detail.extra ?? {}),
    }),
  )

  /**
   * Delete C++ mol objects manually
   * https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html#memory-management
   */
  mol.delete()

  return svg
}

const MoleculeStructure = ({ smiles, className, width, height }: Props) => {
  const [ready, setReady] = useState(() => Boolean(window.RDKit))
  useEffect(() => {
    if (!window.RDKit) {
      RDkitPromise.then(() => {
        setReady(true)
      })
    }
  }, [])

  const svg = useMemo(() => {
    if (ready) {
      return drawSVG(smiles, { width, height })
    }
  }, [ready, height, smiles, width])

  if (!ready) {
    return <div style={{ width, height }}>Loading...</div>
  }

  return (
    <div
      title={smiles}
      className={clsx('molecule-structure-svg', className)}
      style={{ width: '100%', height: '100%' }}
      dangerouslySetInnerHTML={{ __html: svg ?? '' }}
    />
  )
}

export default MoleculeStructure
