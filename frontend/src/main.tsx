import CssBaseline from '@mui/material/CssBaseline'
// import { getGridNumericOperators, getGridStringOperators } from '@mui/x-data-grid'
import { QueryClientProvider } from '@tanstack/react-query'
import React from 'react'
import ReactDOM from 'react-dom/client'
import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import './main.css'
import DetailPage from './pages/DetailPage'
import IndexPage from './pages/IndexPage'
import Queries, { queryClient } from './queries'
import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'

const router = createBrowserRouter([
  {
    path: '/',
    loader: () => {
      queryClient.prefetchQuery(Queries.molecules.getAll({ page: 1 }))
      return null
    },
    element: <IndexPage />,
  },
  {
    path: '/:moleculeId',
    loader: (args) => {
      const { moleculeId } = args.params
      const id = Number(moleculeId)

      if (isNaN(id)) {
        throw new Response('Not Found', { status: 404 })
      }

      queryClient.prefetchQuery(Queries.molecules.detail(id))
      return null
    },
    element: <DetailPage />,
  },
])

// console.log(getGridStringOperators(), getGridNumericOperators())

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <CssBaseline>
      <QueryClientProvider client={queryClient}>
        <RouterProvider router={router} />
      </QueryClientProvider>
    </CssBaseline>
  </React.StrictMode>,
)
