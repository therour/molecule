import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import CircularProgress from '@mui/material/CircularProgress'
import Container from '@mui/material/Container'
import Stack from '@mui/material/Stack'
import Typography from '@mui/material/Typography'
import { useQuery } from '@tanstack/react-query'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import MoleculeStructure from '@/components/MoleculeStructure'
import { ErrorIcon } from '@/components/icons'
import Queries from '@/queries'

export default function DetailPage() {
  const { moleculeId } = useParams<{ moleculeId: string }>()
  const navigate = useNavigate()
  const location = useLocation()

  const moleculeInfo = useQuery(Queries.molecules.detail(Number(moleculeId)))
  return (
    <Container>
      <Container sx={{ paddingTop: 4 }}>
        <Stack gap={4}>
          <Box>
            <Button
              variant="text"
              color="primary"
              onClick={() => (location.key !== 'default' ? navigate(-1) : navigate('/'))}
            >
              {location.key !== 'default' ? 'Back to List of Molecules' : 'List of Molecules'}
            </Button>
          </Box>
          <Box border={1} p={3} borderColor="rgba(224, 224, 224, 1)" minHeight={650}>
            {moleculeInfo.isPending ? (
              <Stack height={600} alignItems="center" justifyContent="center">
                <CircularProgress size="3rem" />
                <Typography variant="h6">Loading Molecule Information...</Typography>
              </Stack>
            ) : moleculeInfo.isError ? (
              <Stack height={600} alignItems="center" justifyContent="center">
                <ErrorIcon color="error" sx={{ height: '3rem', width: '3rem' }} />
                <Typography color="error" variant="h6">
                  Sorry, Something Wrong
                </Typography>
              </Stack>
            ) : (
              <Stack gap={2}>
                <Typography variant="h3">{moleculeInfo.data.data.name}</Typography>
                <Box>
                  <Typography variant="h6">Smiles</Typography>
                  <Typography variant="body1">{moleculeInfo.data.data.smiles}</Typography>
                  <MoleculeStructure smiles={moleculeInfo.data.data.smiles} width={350} height={300} />
                </Box>
                <Box>
                  <Typography variant="h6">Description</Typography>
                  <Typography variant="body1">{moleculeInfo.data.data.description ?? <i>No Description</i>}</Typography>
                </Box>
              </Stack>
            )}
          </Box>
        </Stack>
      </Container>
    </Container>
  )
}
