import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import IconButton, { IconButtonProps } from '@mui/material/IconButton'
import Link from '@mui/material/Link'
import {
  DataGrid,
  DataGridProps,
  GridColDef,
  GridColumnVisibilityModel,
  GridPaginationModel,
  GridRowSelectionModel,
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarFilterButton,
  useGridApiContext,
} from '@mui/x-data-grid'
import { useQuery } from '@tanstack/react-query'
import { useMutation } from '@tanstack/react-query'
import { memo, useEffect, useMemo, useState } from 'react'
import { Link as RouterLink } from 'react-router-dom'
import Api from '@/api'
import NoDataOverlay from '@/components/NoDataOverlay'
import { DeleteIcon, EditIcon } from '@/components/icons'
import Queries, { queryClient } from '@/queries'
import { IMolecule } from '@/types'
import { transformFilterModel, transformSortModel, useDatagridFilter, useDatagridSort } from '@/utils/hooks'
import { EditMoleculeDialog } from './Dialogs'

type Props = {
  /** @defaulValue 4 */
  decimalPrecision?: number
  autoHeight?: DataGridProps['autoHeight']
}

export default function Datatable({ decimalPrecision = 4, ...props }: Props) {
  const columns = useMemo<GridColDef<IMolecule>[]>(() => {
    return [
      {
        field: 'name',
        type: 'string',
        headerName: 'Name',
        width: 200,
        disableColumnMenu: true,
        renderCell: (params) => (
          <Link component={RouterLink} to={`/${params.row.id}`}>
            {params.value}
          </Link>
        ),
      },
      { field: 'smiles', type: 'string', headerName: 'Smiles', width: 300, disableColumnMenu: true },
      {
        field: 'log_p',
        type: 'number',
        headerName: 'Log P',
        flex: 1,
        cellClassName: 'tabular-nums',
        disableColumnMenu: true,
        renderCell: (params) => <span title={params.value}>{params.value.toFixed(decimalPrecision)}</span>,
      },
      {
        field: 'number_of_atoms',
        type: 'number',
        headerName: 'N of Atoms',
        flex: 1,
        disableColumnMenu: true,
        cellClassName: 'tabular-nums',
      },
      {
        field: 'molecular_weight',
        type: 'number',
        headerName: 'Molecular Weight',
        flex: 1,
        disableColumnMenu: true,
        cellClassName: 'tabular-nums',
        renderCell: (params) => <span title={params.value}>{params.value.toFixed(decimalPrecision)}</span>,
      },
      {
        field: 'action',
        type: 'actions',
        hideable: false,
        width: 100,
        getActions: (params) => [<EditButton item={params.row} />, <DeleteButton item={params.row} />],
      },
    ]
  }, [decimalPrecision])

  const [pagination, setPagination] = useState<GridPaginationModel>({ pageSize: 10, page: 0 })
  const { setSortModel, sortQuery } = useDatagridSort(transformSortModel)
  const { setFilterModel, filterQuery } = useDatagridFilter(transformFilterModel)
  const [selectedRows, setSelectedRows] = useState<GridRowSelectionModel>([])
  const [columnVisibilityModel, setColumnVisibilityModel] = useState<GridColumnVisibilityModel>({})
  const hasSelection = selectedRows.length > 0
  const finalColumnVisibilityModel = useMemo(
    () => ({ ...columnVisibilityModel, action: !hasSelection }),
    [columnVisibilityModel, hasSelection],
  )

  const { isFetching, data: res } = useQuery({
    ...Queries.molecules.getAll({
      page: pagination.page + 1,
      sort: sortQuery,
      filter: filterQuery,
    }),
    placeholderData: (data) => data,
  })

  const data = useMemo(() => {
    return {
      rows: res?.data ?? [],
      rowsCount: res?.total ?? 0,
    }
  }, [res])

  return (
    <DataGrid
      {...props}
      slots={{
        toolbar: Toolbar,
        noResultsOverlay: NoDataOverlay,
        noRowsOverlay: NoDataOverlay,
      }}
      loading={isFetching}
      rowCount={data.rowsCount}
      rows={data.rows}
      columns={columns}
      columnVisibilityModel={finalColumnVisibilityModel}
      onColumnVisibilityModelChange={setColumnVisibilityModel}
      checkboxSelection
      disableRowSelectionOnClick
      rowSelectionModel={selectedRows}
      onRowSelectionModelChange={setSelectedRows}
      keepNonExistentRowsSelected
      autoPageSize
      paginationMode="server"
      paginationModel={pagination}
      onPaginationModelChange={setPagination}
      sortingMode="server"
      onSortModelChange={setSortModel}
      filterMode="server"
      onFilterModelChange={setFilterModel}
    />
  )
}

const Toolbar = memo(() => {
  const apiRef = useGridApiContext()
  const [selectedIds, setSelectedIds] = useState<GridRowSelectionModel>([])
  useEffect(() => {
    const unsubscribe = apiRef.current.subscribeEvent('rowSelectionChange', (ids) => setSelectedIds(ids))
    return () => unsubscribe()
  }, [apiRef])

  const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false)

  const { isPending, mutate: handleBulkDelete } = useMutation({
    mutationFn: () => {
      const ids = selectedIds.map((row) => Number(row))
      return Api.molecules.bulkDelete(ids)
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: Queries.molecules.getAll._def })
      setShowDeleteConfirmation(false)
      setSelectedIds([])
    },
  })

  return (
    <>
      <GridToolbarContainer sx={{ padding: '9px', display: 'flex' }}>
        {selectedIds.length > 0 && (
          <Button variant="outlined" color="error" size="small" onClick={() => setShowDeleteConfirmation(true)}>
            Delete {selectedIds.length} Molecule
          </Button>
        )}
        <GridToolbarColumnsButton sx={{ ml: 'auto' }} />
        <GridToolbarFilterButton />
      </GridToolbarContainer>
      <Dialog open={showDeleteConfirmation} onClose={() => setShowDeleteConfirmation(false)}>
        <DialogTitle>Are you sure you want to delete selected molecules?</DialogTitle>
        <DialogContent>
          <DialogContentText>
            You are going to delete {selectedIds.length} molecules. This action cannot be undone.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button disabled={isPending} onClick={() => setShowDeleteConfirmation(false)}>
            Cancel
          </Button>
          <Button disabled={isPending} onClick={() => handleBulkDelete()}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
})

type DeleteButtonProps = IconButtonProps & { item: IMolecule }

const DeleteButton = ({ item, ...props }: DeleteButtonProps) => {
  const [showDialog, setShowDialog] = useState(false)

  const { isPending, mutate: handleDelete } = useMutation({
    mutationFn: (id: number) => Api.molecules.delete(id),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: Queries.molecules.getAll._def })
      setShowDialog(false)
    },
  })

  return (
    <>
      <IconButton {...props} onClick={() => setShowDialog(true)}>
        <DeleteIcon />
      </IconButton>
      <Dialog open={showDialog} onClose={() => setShowDialog(false)}>
        <DialogTitle>Are you sure you want to delete this molecule?</DialogTitle>
        <DialogContent>
          <DialogContentText>You are going to delete {item.name}. This action cannot be undone.</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button disabled={isPending} onClick={() => setShowDialog(false)}>
            Cancel
          </Button>
          <Button disabled={isPending} onClick={() => handleDelete(item.id)}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
const EditButton = ({ item, ...props }: DeleteButtonProps) => {
  const [showDialog, setShowDialog] = useState(false)

  return (
    <>
      <IconButton {...props} onClick={() => setShowDialog(true)}>
        <EditIcon />
      </IconButton>
      <EditMoleculeDialog item={item} open={showDialog} onClose={() => setShowDialog(false)} />
    </>
  )
}
