import { zodResolver } from '@hookform/resolvers/zod'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import Stack from '@mui/material/Stack'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import { useMutation } from '@tanstack/react-query'
import { AxiosProgressEvent, isAxiosError } from 'axios'
import { useCallback, useId, useRef, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import Z from 'zod'
import Api from '@/api'
import CircularLabelProgress from '@/components/CircularLabelProgress'
import FileButton from '@/components/FileButton'
import { CheckCircleIcon, ErrorIcon, WarningIcon } from '@/components/icons'
import Queries, { queryClient } from '@/queries'
import { IMolecule } from '@/types'
import { textFieldProps } from '@/utils/forms'

const addMoleculeSchema = Z.object({
  smiles: Z.string().min(1, 'Formula must be at least 1 character long'),
  // .regex(/^([^J][A-Za-z0-9@+\-[\].()\\/%=#$]+)$/, 'Invalid formula'),
  name: Z.string().max(255, 'Name cannot be longer than 255 characters').nullable(),
  description: Z.string().nullable(),
})
type AddMoleculeForm = Z.infer<typeof addMoleculeSchema>

type DialogProps = { open: boolean; onClose: () => void }
export const AddMoleculeDialog = ({ open, onClose }: DialogProps) => {
  const formId = useId()
  const {
    control,
    reset: resetForm,
    handleSubmit,
    setError,
  } = useForm({
    resolver: zodResolver(addMoleculeSchema),
    defaultValues: {
      smiles: '',
      name: '',
      description: '',
    },
  })

  const addMoleculeMutation = useMutation({
    mutationFn: (data: AddMoleculeForm) =>
      Api.molecules.create({
        smiles: data.smiles,
        name: data.name?.trim() || null,
        description: data.description?.trim() || null,
      }),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: Queries.molecules.getAll._def })
      onClose()
    },
    onError: (error: Error) => {
      if (isAxiosError(error) && error.response?.status === 422) {
        const { errors } = error.response.data
        Object.entries(errors).forEach(([field, message]) => {
          setError(field as keyof AddMoleculeForm, { message: message as string })
        })
      }
    },
  })

  const resetState = () => {
    if (!open) {
      resetForm()
    }
  }

  return (
    <Dialog fullWidth open={open} onClose={onClose} onTransitionExited={resetState}>
      <DialogTitle>Add New Molecule</DialogTitle>
      <DialogContent>
        <form id={formId} onSubmit={handleSubmit((data) => addMoleculeMutation.mutate(data))}>
          <Stack py={4} gap={4}>
            <Controller
              control={control}
              name="smiles"
              render={({ field, fieldState }) => (
                <TextField
                  label="SMILES Formula"
                  {...textFieldProps(field, fieldState)}
                  InputLabelProps={{ shrink: true }}
                  multiline
                  fullWidth
                  variant="outlined"
                  autoFocus
                />
              )}
            />
            <Controller
              control={control}
              name="name"
              render={({ field, fieldState }) => (
                <TextField
                  label="Name"
                  {...textFieldProps(field, fieldState)}
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                  placeholder="default: molecule_{id}"
                />
              )}
            />
            <Controller
              control={control}
              name="description"
              render={({ field, fieldState }) => (
                <TextField
                  label="Description"
                  {...textFieldProps(field, fieldState)}
                  InputLabelProps={{ shrink: true }}
                  multiline
                  minRows={2}
                  fullWidth
                  variant="outlined"
                />
              )}
            />
          </Stack>
        </form>
      </DialogContent>
      <DialogActions>
        <Button type="button" color="secondary" onClick={onClose}>
          Cancel
        </Button>
        <Button form={formId} type="submit">
          Save Molecule
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const editMoleculeSchema = Z.object({
  name: Z.string().min(1, 'Name cannot be empty').max(255, 'Name cannot be longer than 255 characters'),
  description: Z.string().nullable(),
})
type EditMoleculeForm = Z.infer<typeof editMoleculeSchema>

export const EditMoleculeDialog = ({ item, open, onClose }: DialogProps & { item: IMolecule }) => {
  const formId = useId()
  const {
    control,
    reset: resetForm,
    handleSubmit,
    setError,
  } = useForm({
    resolver: zodResolver(editMoleculeSchema),
    defaultValues: {
      name: item.name,
      description: item.description ?? '',
    },
  })

  const editMoleculeMutation = useMutation({
    mutationFn: (data: EditMoleculeForm) =>
      Api.molecules.update({
        id: item!.id,
        name: data.name.trim(),
        description: data.description?.trim() || null,
      }),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: Queries.molecules.getAll._def })
      onClose()
    },
    onError: (error: Error) => {
      if (isAxiosError(error) && error.response?.status === 422) {
        const { errors } = error.response.data
        Object.entries(errors).forEach(([field, message]) => {
          setError(field as keyof EditMoleculeForm, { message: message as string })
        })
      }
    },
  })

  const resetState = () => {
    if (!open) {
      resetForm({ name: item.name, description: item.description })
    }
  }

  return (
    <Dialog fullWidth open={open} onTransitionExited={resetState}>
      <DialogTitle>Edit Molecule</DialogTitle>
      <DialogContent>
        <form id={formId} onSubmit={handleSubmit((data) => editMoleculeMutation.mutate(data))}>
          <Stack py={4} gap={4}>
            <Controller
              control={control}
              name="name"
              render={({ field, fieldState }) => (
                <TextField
                  label="Name"
                  {...textFieldProps(field, fieldState)}
                  InputLabelProps={{ shrink: true }}
                  fullWidth
                  variant="outlined"
                />
              )}
            />
            <Controller
              control={control}
              name="description"
              render={({ field, fieldState }) => (
                <TextField
                  label="Description"
                  {...textFieldProps(field, fieldState)}
                  InputLabelProps={{ shrink: true }}
                  multiline
                  minRows={2}
                  fullWidth
                  variant="outlined"
                />
              )}
            />
          </Stack>
        </form>
      </DialogContent>
      <DialogActions>
        <Button type="button" color="secondary" onClick={onClose}>
          Cancel
        </Button>
        <Button form={formId} type="submit">
          Save Molecule
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export const ActionImportButton = () => {
  const [showDialog, setShowDialog] = useState(false)
  const [progress, setProgress] = useState(0)
  const [isIndeterminate, setIsIndeterminate] = useState(false)
  const abortControllerRef = useRef<AbortController | undefined>()
  const inputRef = useRef<HTMLInputElement>(null)

  const handleUploadProgress = useCallback((evt: AxiosProgressEvent) => {
    const { loaded, total } = evt
    // we show indeterminate progress if total is undefined
    if (total === undefined) {
      setIsIndeterminate(true)
      return
    }

    const value = (loaded / total) * 100
    setProgress(value)
  }, [])

  const {
    mutate: handleImport,
    data: response,
    error,
    isPending,
    isError,
    isSuccess,
    reset: resetMutation,
  } = useMutation({
    mutationFn: (file: File) => {
      abortControllerRef.current = new AbortController()
      return Api.molecules.import(file, {
        signal: abortControllerRef.current.signal,
        onUploadProgress: handleUploadProgress,
      })
    },
    onMutate: () => {
      setShowDialog(true)
    },
    onSuccess: (response) => {
      if (response.data.success_count > 0) {
        queryClient.invalidateQueries({ queryKey: Queries.molecules.getAll._def })
      }
    },
    onSettled: () => {
      if (inputRef.current) {
        inputRef.current.value = ''
      }
    },
  })

  const reset = () => {
    setProgress(0)
    setIsIndeterminate(false)
    resetMutation()
  }

  const handleCancel = () => {
    abortControllerRef.current?.abort()
    setShowDialog(false)
    reset()
  }

  return (
    <>
      <FileButton
        inputRef={inputRef}
        variant="outlined"
        color="primary"
        onChange={(file) => file && handleImport(file)}
      >
        Import Molecule (.smi)
      </FileButton>
      <Dialog
        fullWidth
        maxWidth="xs"
        open={showDialog}
        onClose={() => {
          if (isPending) return
          setShowDialog(false)
        }}
        onKeyUp={(e) => {
          if (e.key === 'Escape' && isPending && progress < 100) {
            handleCancel()
          } else if (e.key === 'Enter' && !isPending) {
            setShowDialog(false)
          }
        }}
        onTransitionExited={() => reset()}
      >
        {isPending ? (
          <>
            <DialogTitle sx={{ textAlign: 'center' }}>Uploading files...</DialogTitle>
            <DialogContent>
              <Stack alignItems="center" justifyContent="center">
                {isIndeterminate ? (
                  <CircularLabelProgress />
                ) : (
                  <CircularLabelProgress variant="determinate" value={progress} />
                )}
              </Stack>
            </DialogContent>
          </>
        ) : isSuccess ? (
          <>
            <DialogTitle sx={{ textAlign: 'center' }}>Import Finished</DialogTitle>
            <Stack component={DialogContent} gap={3}>
              {response.data.success_count > 0 && (
                <Stack direction="row" alignItems="center" gap={1}>
                  <CheckCircleIcon color="success" />
                  <Typography lineHeight={0} variant="subtitle1">
                    {response.data.success_count} SMILES successfully imported
                  </Typography>
                </Stack>
              )}

              {response.data.already_exists.length > 0 && (
                <Box>
                  <Stack direction="row" alignItems="center" gap={1}>
                    <WarningIcon color="warning" />
                    <Typography lineHeight={0} variant="subtitle1">
                      Found {response.data.already_exists.length} SMILES are already exists
                    </Typography>
                  </Stack>
                  <Box component="ul">
                    {response.data.already_exists.map((smiles) => (
                      <Typography component="li" fontSize={12} key={smiles}>
                        {smiles}
                      </Typography>
                    ))}
                  </Box>
                </Box>
              )}

              {response.data.invalid.length > 0 && (
                <Box>
                  <Stack direction="row" alignItems="center" gap={1}>
                    <ErrorIcon color="error" />
                    <Typography color="error" lineHeight={0} variant="subtitle1">
                      Found {response.data.invalid.length} invalid SMILES
                    </Typography>
                  </Stack>
                  <Typography variant="subtitle1" color="error"></Typography>
                  <Box component="ul">
                    {response.data.invalid.map((smiles) => (
                      <Typography component="li" fontSize={12} key={smiles}>
                        {smiles}
                      </Typography>
                    ))}
                  </Box>
                </Box>
              )}
            </Stack>
          </>
        ) : isError ? (
          <>
            <DialogTitle
              component={Stack}
              justifyContent="center"
              alignItems="center"
              direction="row"
              gap={1}
              color="error"
              sx={{ textAlign: 'center' }}
            >
              <ErrorIcon color="error" />
              Import Failed
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                {isAxiosError(error) && error.response?.status === 422
                  ? error.response.data.errors.file
                  : 'Internal Server Error'}
              </DialogContentText>
            </DialogContent>
          </>
        ) : null}
        <DialogActions>
          <Button disabled={isPending && progress === 100} type="button" color="secondary" onClick={handleCancel}>
            {isPending ? 'Cancel' : 'Ok'}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
