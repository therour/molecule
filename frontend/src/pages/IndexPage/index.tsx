import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import Container from '@mui/material/Container'
import Stack from '@mui/material/Stack'
import { useState } from 'react'
import Datatable from './Datatable'
import { ActionImportButton, AddMoleculeDialog } from './Dialogs'

function IndexPage() {
  const [showAddDialog, setShowAddDialog] = useState(false)

  return (
    <Container sx={{ paddingTop: 4 }}>
      <Stack gap={4}>
        <Stack direction="row" gap={2}>
          <Button variant="contained" color="primary" onClick={() => setShowAddDialog(true)}>
            Add Molecule
          </Button>
          <AddMoleculeDialog open={showAddDialog} onClose={() => setShowAddDialog(false)} />
          <ActionImportButton />
        </Stack>
        <Box height={650}>
          <Datatable decimalPrecision={4} />
        </Box>
      </Stack>
    </Container>
  )
}

export default IndexPage
