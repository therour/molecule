import { createQueryKeyStore } from '@lukemorales/query-key-factory'
import { QueryClient } from '@tanstack/react-query'
import Api, { GetAllMoleculesParams } from '@/api'

// eslint-disable-next-line react-refresh/only-export-components
export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnReconnect: false,
      retry: 0,
      staleTime: Infinity, // Always cache the query, please invalidate manually to refetch
    },
  },
})

const Queries = createQueryKeyStore({
  molecules: {
    getAll: (params: GetAllMoleculesParams) => ({
      queryKey: [{ params }],
      queryFn: () => Api.molecules.getAll(params),
    }),
    detail: (moleculeId: number) => ({
      queryKey: [moleculeId],
      queryFn: () => Api.molecules.find(moleculeId),
    }),
  },
})

export default Queries
