export interface IMolecule {
  id: number
  name: string
  description?: string
  smiles: string
  log_p: number
  number_of_atoms: number
  molecular_weight: number
}

export type FilterQueryObject = {
  [field: string]: { operator: string; value: unknown }
}
