import { ControllerFieldState, ControllerRenderProps, FieldPath, FieldValues } from 'react-hook-form'

export const textFieldProps = <TFieldValues extends FieldValues, TName extends FieldPath<TFieldValues>>(
  field: ControllerRenderProps<TFieldValues, TName>,
  fieldState: ControllerFieldState,
) => {
  return {
    inputRef: field.ref,
    ...field,
    ...(fieldState.error
      ? {
          error: true,
          helperText: fieldState.error?.message ?? undefined,
        }
      : {}),
  }
}
