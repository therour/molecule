import { GridFilterModel, GridSortModel } from '@mui/x-data-grid'
import { useMemo, useState } from 'react'
import { FilterQueryObject } from '@/types'

export const transformSortModel = (sortedModel: GridSortModel) =>
  sortedModel.length > 0 ? sortedModel.map((s) => `${s.sort === 'desc' ? '-' : ''}${s.field}`).join(',') : undefined

export const useDatagridSort = (transform: (sortModel: GridSortModel) => string | undefined) => {
  const [sortModel, setSortModel] = useState<GridSortModel>([])
  const sortQuery = useMemo(() => transform(sortModel), [sortModel, transform])

  return { sortModel, setSortModel, sortQuery }
}

export const transformFilterModel = (filterModel: GridFilterModel) => {
  if (filterModel.items.length === 0) {
    return undefined
  }
  return filterModel.items.reduce((obj, item) => {
    obj[item.field] = { operator: item.operator, value: item.value }
    return obj
  }, {} as FilterQueryObject)
}

export const useDatagridFilter = (transform: (filterModel: GridFilterModel) => FilterQueryObject | undefined) => {
  const [filterModel, setFilterModel] = useState<GridFilterModel>(() => ({
    items: [],
  }))

  const filterQuery = useMemo(() => transform(filterModel), [filterModel, transform])
  const anotherTransform = useMemo(() => {
    if (filterModel.items.length === 0) {
      return undefined
    }

    return filterModel.items.reduce((obj, item) => {
      obj[item.field] = { operator: item.operator, value: item.value }
      return obj
    }, {} as FilterQueryObject)
  }, [filterModel])

  return { filterModel, setFilterModel, filterQuery, anotherTransform }
}
