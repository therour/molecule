/// <reference types="vite/client" />
import type { RDKitModule } from '@rdkit/rdkit'

declare global {
  interface Window {
    RDKit?: RDKitModule
  }
}

interface ImportMetaEnv {
  readonly VITE_API_URL: string
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
